# Apify Tutorial II



## Quiz answers
<!-- toc start -->
- What types of proxies does the Apify Proxy include? What are the main differences between them?
  - Apify Proxy include : Datacenter proxy ,Residential proxy ,Google SERP proxy
  - Datacenter proxy : uses data centers to mask IP address
  - Residential proxy : uses IP addresses located in homes and offices and have a lowest chance of blocking
  - Google SERP proxy : download and extract data from Google Search engine result pages (SERPs)


- Which proxies (proxy groups) can users access with the Apify Proxy trial? How long does this trial last?
  - BUYPROXIES94952 and GOOGLESERP
  - It last a month
- Does it make sense to rotate proxies when you are logged in?
  - Yes
- Construct a proxy URL that will select proxies only from the US (without specific groups).
  - 'http://auto,country-US:<YOUR_PROXY_PASSWORD>@proxy.apify.com:8000'
- What do you need to do to rotate proxies (one proxy usually has one IP)? How does this differ for Cheerio Scraper and Puppeteer Scraper?
  - To rotate proxies you need to set proxy configuration
  - So far didn't see any differ
- Name a few different ways a website can prevent you from scraping it.
  - IP detection
  - IP rate limiting
  - Browser detection
  - Tracking user behavior
 <!-- toc end -->



